# Employee & EventService uses Spring Boot and micro services
![Requirements](requirements.png)

# What has been done
All required parts, bonus parts: 

1.  Use Swagger (https://swagger.io) to expose employeeservice and eventservice endpoints.
2.  Add authentication to access create, update and delete employeeservice endpoints.


# Prerequisite

1. Installations: Install latest Oracle JDK 8, download at <http://www.oracle.com/technetwork/java/javase/downloads/index.html>
2. Install Apache Maven 3.6.1
3. Install ActiveMQ at least 5.15.9 at <https://activemq.apache.org/components/classic/download/>

# Build and run the application
1.  Run ActiveMQ by running apache-activemq-5.15.9\bin\win64\activemq.bat. **It is a must!!!**
2.  Use the command `mvn clean install` in the project folder `employee-event-parent` to build and run all unit tests (33) and integration tests (36). All integration tests in `employee-service` will run first and send events to ActiveMQ and stops. After that, the app `event-service` will run and will process all messages receives by ActiveMQ and insert data into database and run its integration test.  
Note: Integration tests (IT) must be run in order (if run manually): employee-service's ITs run first, and then event-service's IT runs because employee-service will create employee events, then event-service will get events from ActiveMQ.
3.  Use the command `mvn spring-boot:run` in the project folder `employee-service` to run that micro service, it will listen at the port 8080  
4.  Use the command `mvn spring-boot:run` in the project folder `event-service` to run that micro service, it will listen at the port 8085
5.  We can use [Postman](https://www.getpostman.com/ "Postman") to test REST APIs, import the Postman collection [EmployeeEventService.postman_collection.json](EmployeeEventService.postman_collection.json).   
Steps:
    *   Run the API **Login** to obtain the JWT (Json Web Token) authentication token. username is **admin**, password is **admin**
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **createDepartment** and **Send**
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **createEmployee** and **Send**
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **updateEmployee** and **Send**
    *   Send a request to the API **findEmployeeById** 
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **deleteEmployeeById** and **Send**
    *   Send a request to the API **findAllEvents**
6.  Access the Swagger API documentation at http://localhost:8080/swagger-ui.html and http://localhost:8085/swagger-ui.html
7.  Access the embedded H2 database console at http://localhost:8080/h2-console and http://localhost:8085/h2-console    


# Notes about technologies and architecture
*   2 micro services: `employee-service` and `event-service`
*   Spring Boot 2.1.4 for the app with Spring Rest, Spring Security, Json Web Token (jjwt), Spring Data JPA, Bean Validation 
*   Embedded database H2
*   Message broker Apache ActiveMQ 5.15
*   Unit test with jmockit for mocking, integration test with REST Assured. Run unit tests in Eclipse: You must specify a JVM argument to jmockit.jar, my PC is: -javaagent:C:/Users/Duc/.m2/repository/org/jmockit/jmockit/1.46/jmockit-1.46.jar

# Notes about code
*   All configuration classes are in the packages com.mycompany.employeeevent.config. When the app `employee-service` is initialized, the class InitData will insert a user with username **admin** and password **admin** into the database, table **user**, to authenticate later.  
*   Event producer and consumer to and from JMS queue (Apache ActiveMQ) are implemented with Apache Camel, please look at the class **CamelRoutes**.  
Producer: `from("direct:employee-event").to("activemq:queue:EMPLOYEE.EVENT?transacted=true&lazyCreateTransactionManager=false")`,   
Consumer: `from("activemq:queue:EMPLOYEE.EVENT?transacted=true&lazyCreateTransactionManager=false").bean(eventService, "processEmployeeEvent")`    
*   Message broker Apache ActiveMQ
*   Unit test with jmockit for mocking, integration test with REST Assured
