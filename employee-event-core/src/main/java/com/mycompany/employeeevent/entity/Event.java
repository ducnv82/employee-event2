package com.mycompany.employeeevent.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.mycompany.employeeevent.model.EventType;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import static javax.persistence.EnumType.STRING;

@Entity(name = "event")
@EntityListeners(AuditingEntityListener.class)
public class Event {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid", updatable = false)
    private UUID id;

    @Column(name = "event_type", length = 20)
    @Enumerated(STRING)
    private EventType eventType;

    @Column(name = "old_employee", columnDefinition = "TEXT")
    private String oldEmployee;

    @Column(name = "new_employee", columnDefinition = "TEXT")
    private String newEmployee;

    @Column(name = "created_date", nullable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    public Event() {}

    public Event(final EventType eventType, final String oldEmployee, final String newEmployee) {
        this.eventType = eventType;
        this.newEmployee = newEmployee;
        this.oldEmployee = oldEmployee;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(final EventType eventType) {
        this.eventType = eventType;
    }

    public String getNewEmployee() {
        return newEmployee;
    }

    public void setNewEmployee(final String employee) {
        newEmployee = employee;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public String getOldEmployee() {
        return oldEmployee;
    }

    public void setOldEmployee(final String oldEmployee) {
        this.oldEmployee = oldEmployee;
    }
}
