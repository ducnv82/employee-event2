package com.mycompany.employeeevent.config;

import java.util.Optional;

import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableTransactionManagement
public class CoreConfig {

    @Bean
    public AuditorAware<String> auditorAware() {
        return () -> Optional.of("System");
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }
}
