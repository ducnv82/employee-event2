package com.mycompany.employeeevent.model;

public enum EventType {
    CREATED, UPDATED, DELETED;
}
