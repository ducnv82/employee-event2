package com.mycompany.employeeevent.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import static org.springframework.beans.BeanUtils.copyProperties;

@ApiModel
public class Employee implements Serializable {

    @ApiModelProperty(example = "0808f043-5848-44b5-9584-ad5cfc1f65ef", notes = "A generated immutable universally unique identifier (UUID)")
    private UUID id;

    @ApiModelProperty(example = "employee1@gmail.com", notes = "Employee's email")
    private String email;

    @ApiModelProperty(example = "Robin Hood", notes = "Employee's full name")
    private String fullName;

    @ApiModelProperty(example = "1", notes = "Employee's department id")
    private Long departmentId;

    @ApiModelProperty(example = "2000-04-22", notes = "Employee's birthday.")
    private LocalDate birthday;

    @ApiModelProperty(notes = "Employee's departmnet. Maybe empty. It only has value in the API's response /api/employees/{id}", allowEmptyValue = true)
    private Department department;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(final Long departmentId) {
        this.departmentId = departmentId;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(final LocalDate birthday) {
        this.birthday = birthday;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(final Department department) {
        this.department = department;
    }

    public static Employee of(final com.mycompany.employeeevent.entity.Employee entity) {
        final Employee employee = new Employee();
        copyProperties(entity, employee);
        return employee;
    }
}
