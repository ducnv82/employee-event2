package com.mycompany.employeeevent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import static org.springframework.beans.BeanUtils.copyProperties;

@ApiModel
public class Department {

    @ApiModelProperty(example = "1", notes = "Auto incremented value, starting with 1")
    private long id;

    @ApiModelProperty(example = "department 1", notes = "Department's name")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public static Department of(final com.mycompany.employeeevent.entity.Department entity) {
        final Department department = new Department();
        copyProperties(entity, department);
        return department;
    }
}
