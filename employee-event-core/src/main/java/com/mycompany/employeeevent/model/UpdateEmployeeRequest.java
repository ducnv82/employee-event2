package com.mycompany.employeeevent.model;


import java.time.LocalDate;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mycompany.employeeevent.jackson.LocalDateDeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class UpdateEmployeeRequest {

    @NotNull
    @ApiModelProperty(example = "0808f043-5848-44b5-9584-ad5cfc1f65ef", notes = "A generated immutable universally unique identifier (UUID)")
    private UUID id;

    @NotBlank
    @Email
    @ApiModelProperty(required = true, example = "employee2@gmail.com", notes = "Email must be unique, i.e. 2 employees cannot have the same email.")
    private String email;

    @ApiModelProperty(notes = "Employee's full name", example = "Robin Hood")
    private String fullName;

    @NotNull
    @ApiModelProperty(notes = "Employee's department id", required = true, example = "1")
    private Long departmentId;

    @JsonDeserialize(using = LocalDateDeSerializer.class)
    @ApiModelProperty(notes = "Employee's birthday. A date string in format yyyy-MM-dd", example = "2001-01-01")
    private LocalDate birthday;

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(final LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(final Long departmentId) {
        this.departmentId = departmentId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }
}
