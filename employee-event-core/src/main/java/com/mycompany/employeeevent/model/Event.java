package com.mycompany.employeeevent.model;

import java.time.LocalDateTime;
import java.util.UUID;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Event {

    @ApiModelProperty(example = "0808f043-5848-44b5-9584-ad5cfc1f65ef", notes = "A generated immutable universally unique identifier (UUID)")
    private UUID id;

    @ApiModelProperty(example = "DELETED", allowableValues = "CREATED, UPDATED, DELETED", dataType = "string", notes = "Type of event")
    private EventType eventType;

    @ApiModelProperty(notes = "Value of old employee in case of UPDATED or DELETED")
    private Employee oldEmployee;

    @ApiModelProperty(notes = "Value of old employee in case of CREATED or UPDATED")
    private Employee newEmployee;

    @ApiModelProperty(example = "2000-01-11T20:32:25.676", notes = "Local date time when the event was created")
    private LocalDateTime createdDate;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(final EventType eventType) {
        this.eventType = eventType;
    }

    public Employee getOldEmployee() {
        return oldEmployee;
    }

    public void setOldEmployee(final Employee oldEmployee) {
        this.oldEmployee = oldEmployee;
    }

    public Employee getNewEmployee() {
        return newEmployee;
    }

    public void setNewEmployee(final Employee newEmployee) {
        this.newEmployee = newEmployee;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(final LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }


}
