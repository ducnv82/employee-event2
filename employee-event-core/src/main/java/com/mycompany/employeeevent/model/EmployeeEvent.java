package com.mycompany.employeeevent.model;

import java.io.Serializable;

public class EmployeeEvent implements Serializable {

    private EventType eventType;
    private Employee oldEmployee;
    private Employee newEmployee;

    public EmployeeEvent(final EventType eventType, final Employee oldEmployee, final Employee newEmployee) {
        this.eventType = eventType;
        this.oldEmployee = oldEmployee;
        this.newEmployee = newEmployee;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(final EventType eventType) {
        this.eventType = eventType;
    }

    public Employee getOldEmployee() {
        return oldEmployee;
    }

    public void setOldEmployee(final Employee oldEmployee) {
        this.oldEmployee = oldEmployee;
    }

    public Employee getNewEmployee() {
        return newEmployee;
    }

    public void setNewEmployee(final Employee employee) {
        newEmployee = employee;
    }
}
