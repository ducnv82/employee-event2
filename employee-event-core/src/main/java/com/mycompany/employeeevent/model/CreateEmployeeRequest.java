package com.mycompany.employeeevent.model;


import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mycompany.employeeevent.jackson.LocalDateDeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class CreateEmployeeRequest {

    @NotBlank
    @Email
    @ApiModelProperty(required = true, example = "employee1@gmail.com")
    private String email;

    @ApiModelProperty(notes = "Employee's full name", example = "Robin Hood")
    private String fullName;

    @NotNull
    @ApiModelProperty(notes = "Employee's department id", required = true, example = "1")
    private Long departmentId;

    @JsonDeserialize(using = LocalDateDeSerializer.class)
    @ApiModelProperty(notes = "Employee's birthday. A date string in format yyyy-MM-dd", example = "2001-01-01")
    private LocalDate birthday;

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(final LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(final Long departmentId) {
        this.departmentId = departmentId;
    }
}
