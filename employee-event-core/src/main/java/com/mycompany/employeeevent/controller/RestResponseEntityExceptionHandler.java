package com.mycompany.employeeevent.controller;

import java.util.Optional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.mycompany.employeeevent.model.ErrorResponse;
import org.springframework.beans.TypeMismatchException;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static java.lang.System.currentTimeMillis;
import static javax.servlet.RequestDispatcher.ERROR_EXCEPTION;
import static javax.servlet.RequestDispatcher.ERROR_MESSAGE;
import static javax.servlet.RequestDispatcher.ERROR_REQUEST_URI;
import static javax.servlet.RequestDispatcher.ERROR_STATUS_CODE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    protected ResponseEntity<Object> error(final HttpServletRequest request) {
        final Optional<Exception> exceptionOptional =
                Optional.ofNullable((Exception) request.getAttribute(ERROR_EXCEPTION));

        return exceptionOptional.isPresent() ? handleExceptionError(exceptionOptional.get(), request)
                                                      : handleNonExceptionError(request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(final EntityNotFoundException ex, final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(NOT_FOUND, request, ex.getMessage()), new HttpHeaders(),
                                       NOT_FOUND, request);
    }

    @ExceptionHandler(EntityExistsException.class)
    protected ResponseEntity<Object> handleEntityExistsException(final EntityExistsException ex, final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(CONFLICT, request, ex.getMessage()), new HttpHeaders(),
                                       CONFLICT, request);
    }

    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleOther(final RuntimeException ex, final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(INTERNAL_SERVER_ERROR, request, ex.getMessage()),
                                       new HttpHeaders(), INTERNAL_SERVER_ERROR, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex,
                                                                  final HttpHeaders headers, final HttpStatus status,
                                                                  final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(BAD_REQUEST, request, ex.getMessage()), headers,
                                       BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
                                                                  final HttpHeaders headers, final HttpStatus status,
                                                                  final WebRequest request) {
        final StringBuilder messagesBuilder = new StringBuilder();
        for (final FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            messagesBuilder.append("Field '").append(fieldError.getField()).append("' ").append(fieldError.getDefaultMessage())
                    .append(", invalid value: '").append(fieldError.getRejectedValue()).append("'. ");
        }
        for (final ObjectError objectError : ex.getBindingResult().getGlobalErrors()) {
            messagesBuilder.append("Object '").append(objectError.getObjectName()).append("' ")
                    .append(objectError.getDefaultMessage()).append(". ");
        }

        return handleExceptionInternal(ex, createErrorResponse(BAD_REQUEST, request, messagesBuilder.toString()),
                                       new HttpHeaders(), BAD_REQUEST, request);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex, final WebRequest request) {
        final StringBuilder messagesBuilder = new StringBuilder();
        for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            messagesBuilder.append(violation.getRootBeanClass().getName()).append(" ").append(violation.getPropertyPath())
                    .append(": ").append(violation.getMessage()).append(", invalid value: '")
                    .append(violation.getInvalidValue()).append("'. ");
        }

        return handleExceptionInternal(ex, createErrorResponse(BAD_REQUEST, request, messagesBuilder.toString()),
                                       new HttpHeaders(), BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            final MissingServletRequestParameterException ex,
            final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(BAD_REQUEST, request, ex.getMessage()), headers,
                                       BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(final MissingPathVariableException ex,
                                                               final HttpHeaders headers, final HttpStatus status,
                                                               final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(BAD_REQUEST, request, ex.getMessage()), headers,
                                       BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(final ServletRequestBindingException ex,
                                                                          final HttpHeaders headers,
                                                                          final HttpStatus status,
                                                                          final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(BAD_REQUEST, request, ex.getMessage()), headers,
                                       BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers,
                                                        final HttpStatus status, final WebRequest request) {
        final String message = "Parameter name: " + ((MethodArgumentTypeMismatchException) ex).getName() + ", value: " +
                             ex.getValue() + ", required type: " + ex.getRequiredType() + ". " + ex.getMessage();
        final ErrorResponse errorResponse = new ErrorResponse(currentTimeMillis(), BAD_REQUEST.value(),
                    BAD_REQUEST.getReasonPhrase(), message, ((ServletWebRequest) request).getRequest().getRequestURI());

        return handleExceptionInternal(ex, errorResponse, headers, BAD_REQUEST, request);
    }

    protected ErrorResponse createErrorResponse(final HttpStatus status, final WebRequest request, final String message) {
        return new ErrorResponse(currentTimeMillis(), status.value(), status.getReasonPhrase(), message,
                                 ((ServletWebRequest) request).getRequest().getRequestURI());
    }

    protected ResponseEntity<Object> handleNonExceptionError(final HttpServletRequest request) {
        final int errorStatusCode = (Integer) request.getAttribute(ERROR_STATUS_CODE);
        final String errorMessage = (String) request.getAttribute(ERROR_MESSAGE);
        final String errorRequestURI = (String) request.getAttribute(ERROR_REQUEST_URI);
        final HttpStatus httpStatus = HttpStatus.valueOf(errorStatusCode);

        final ErrorResponse errorResponse = new ErrorResponse(currentTimeMillis(), httpStatus.value(),
                                                              httpStatus.getReasonPhrase(), errorMessage,
                                                              errorRequestURI);

        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    protected ResponseEntity<Object> handleExceptionError(final Exception exception, final HttpServletRequest request) {
        return handleOtherException(exception, request);
    }

    protected ResponseEntity<Object> handleOtherException(final Exception ex,
                                                          final HttpServletRequest request) {
        final HttpStatus httpStatus = INTERNAL_SERVER_ERROR;
        final ErrorResponse errorResponse = new ErrorResponse(currentTimeMillis(), httpStatus.value(),
                                                              httpStatus.getReasonPhrase(), ex.getMessage(),
                                                              request.getRequestURI());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }
}
