package com.mycompany.employeeevent.service;

import java.time.LocalDate;
import java.util.UUID;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.mycompany.employeeevent.entity.Event;
import com.mycompany.employeeevent.model.Employee;
import com.mycompany.employeeevent.model.EmployeeEvent;
import com.mycompany.employeeevent.repository.EventRepository;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Before;
import org.junit.Test;

import static com.mycompany.employeeevent.model.EventType.UPDATED;
import static org.junit.Assert.assertEquals;

public class EventServiceImplTest {

    @Tested
    private EventServiceImpl eventService;

    @Injectable
    private EventRepository eventRepository;

    @Injectable
    private Gson mockedGson;

    private Employee oldEmployee;
    private Employee newEmployee;
    private Gson actualGson;
    private EmployeeEvent employeeEvent;
    private Event event;

    @Before
    public void setUp() {
        oldEmployee = createOldEmployee();
        newEmployee = createNewEmployee();
        actualGson = new Gson();
        employeeEvent = new EmployeeEvent(UPDATED, oldEmployee, newEmployee);
        event = createEvent();
    }

    @Test
    public void testProcessEmployeeEvent() {
        new Expectations() {
            {
                mockedGson.toJson(oldEmployee);
                result = actualGson.toJson(oldEmployee);

                mockedGson.toJson(newEmployee);
                result = actualGson.toJson(newEmployee);
            }
        };

        eventService.processEmployeeEvent(employeeEvent);

        new Verifications() {{
            com.mycompany.employeeevent.entity.Event eventToVerify;
            eventRepository.save(eventToVerify = withCapture());

            final Employee oldEmployeeToVerify = actualGson.fromJson(eventToVerify.getOldEmployee(), Employee.class);
            final Employee newEmployeeToVerify = actualGson.fromJson(eventToVerify.getNewEmployee(), Employee.class);

            assertEquals(UPDATED, eventToVerify.getEventType());
            assertEquals(oldEmployee.getBirthday(), oldEmployeeToVerify.getBirthday());
            assertEquals(oldEmployee.getDepartmentId(), oldEmployeeToVerify.getDepartmentId());
            assertEquals(oldEmployee.getEmail(), oldEmployeeToVerify.getEmail());
            assertEquals(oldEmployee.getFullName(), oldEmployeeToVerify.getFullName());
            assertEquals(oldEmployee.getId(), oldEmployeeToVerify.getId());

            assertEquals(newEmployee.getBirthday(), newEmployeeToVerify.getBirthday());
            assertEquals(newEmployee.getDepartmentId(), newEmployeeToVerify.getDepartmentId());
            assertEquals(newEmployee.getEmail(), newEmployeeToVerify.getEmail());
            assertEquals(newEmployee.getFullName(), newEmployeeToVerify.getFullName());
            assertEquals(newEmployee.getId(), newEmployeeToVerify.getId());
        }};
    }

    @Test
    public void testFindAllEvents() {
        final Employee eventOldEmployee = actualGson.fromJson(event.getOldEmployee(), Employee.class);
        final Employee eventNewEmployee = actualGson.fromJson(event.getNewEmployee(), Employee.class);

        new Expectations() {
            {
                eventRepository.findAllOrderByCreatedDate();
                result = ImmutableList.of(event);

                mockedGson.fromJson(event.getOldEmployee(), Employee.class);
                result = eventOldEmployee;

                mockedGson.fromJson(event.getNewEmployee(), Employee.class);
                result = eventNewEmployee;
            }
        };

        final com.mycompany.employeeevent.model.Event eventToVerify = eventService.findAllEvents().get(0);
        assertEquals(event.getEventType(), eventToVerify.getEventType());
        assertEquals(event.getCreatedDate(), eventToVerify.getCreatedDate());
        assertEquals(event.getId(), eventToVerify.getId());

        assertEquals(eventOldEmployee.getBirthday(), eventToVerify.getOldEmployee().getBirthday());
        assertEquals(eventOldEmployee.getDepartmentId(), eventToVerify.getOldEmployee().getDepartmentId());
        assertEquals(eventOldEmployee.getEmail(), eventToVerify.getOldEmployee().getEmail());
        assertEquals(eventOldEmployee.getFullName(), eventToVerify.getOldEmployee().getFullName());
        assertEquals(eventOldEmployee.getId(), eventToVerify.getOldEmployee().getId());

        assertEquals(eventNewEmployee.getBirthday(), eventToVerify.getNewEmployee().getBirthday());
        assertEquals(eventNewEmployee.getDepartmentId(), eventToVerify.getNewEmployee().getDepartmentId());
        assertEquals(eventNewEmployee.getEmail(), eventToVerify.getNewEmployee().getEmail());
        assertEquals(eventNewEmployee.getFullName(), eventToVerify.getNewEmployee().getFullName());
        assertEquals(eventNewEmployee.getId(), eventToVerify.getNewEmployee().getId());
    }

    private Employee createOldEmployee() {
        final Employee employee = new Employee();
        employee.setId(UUID.randomUUID());
        employee.setBirthday(LocalDate.of(1900, 1, 1));
        employee.setDepartmentId(1l);
        employee.setEmail("robin.hood@aol.com");
        employee.setFullName("Robin Hood");
        return employee;
    }

    private Employee createNewEmployee() {
        final Employee employee = new Employee();
        employee.setId(UUID.randomUUID());
        employee.setBirthday(LocalDate.of(1902, 2, 2));
        employee.setDepartmentId(2l);
        employee.setEmail("new.robin.hood@aol.com");
        employee.setFullName("New Robin Hood");
        return employee;
    }

    private Event createEvent() {
        final Event event = new Event();
        event.setEventType(UPDATED);
        event.setId(UUID.randomUUID());
        event.setOldEmployee(actualGson.toJson(oldEmployee));
        event.setNewEmployee(actualGson.toJson(newEmployee));
        return event;
    }
}
