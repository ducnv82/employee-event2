package com.mycompany.employeeevent.controller;


import java.time.LocalDateTime;

import com.mycompany.employeeevent.EventServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static java.time.LocalDateTime.parse;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventServiceApplication.class, webEnvironment = DEFINED_PORT)
public class EventControllerIT {

    @Test
    public void testFindAllEvents_orderByCreatedDate() {
        final String event1CreatedDateString = given().contentType(JSON)
                .accept(JSON)
                .when().get("http://localhost:8085/api/events").then().statusCode(200)
                .body("size", greaterThan(0))
                .body("[0]['eventType']", notNullValue())
                .body("[0]['createdDate']", notNullValue())
                .extract().path("[0]['createdDate']");

        //assert order by createdDate
        final LocalDateTime event1CreatedDate = parse(event1CreatedDateString);

        final String event2CreatedDateString = given().contentType(JSON).accept(JSON)
                .when().get("http://localhost:8085/api/events").then().extract().path("[1]['createdDate']");
        final LocalDateTime event2CreatedDate = parse(event2CreatedDateString);

        final String event3CreatedDateString = given().contentType(JSON).accept(JSON)
                .when().get("http://localhost:8085/api/events").then().extract().path("[2]['createdDate']");
        final LocalDateTime event3CreatedDate = parse(event3CreatedDateString);

        //Event insertion into db runs too fast so createdDate of 2 adjacent events may be equal.
        assertThat(event3CreatedDate, greaterThanOrEqualTo(event2CreatedDate));
        assertThat(event2CreatedDate, greaterThanOrEqualTo(event1CreatedDate));
    }
}
