package com.mycompany.employeeevent.service;

import java.util.List;

import com.mycompany.employeeevent.model.EmployeeEvent;
import com.mycompany.employeeevent.model.Event;

public interface EventService {

    void processEmployeeEvent(EmployeeEvent employeeEvent);

    List<Event> findAllEvents();
}
