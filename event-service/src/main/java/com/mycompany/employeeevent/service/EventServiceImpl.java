package com.mycompany.employeeevent.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.mycompany.employeeevent.entity.Event;
import com.mycompany.employeeevent.model.Employee;
import com.mycompany.employeeevent.model.EmployeeEvent;
import com.mycompany.employeeevent.repository.EventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EventServiceImpl implements EventService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventServiceImpl.class);

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private Gson gson;

    @Override
    @Transactional
    public void processEmployeeEvent(final EmployeeEvent employeeEvent) {
        LOGGER.debug("processEmployeeEvent, employeeEvent: {}", gson.toJson(employeeEvent));

        eventRepository.save(new Event(employeeEvent.getEventType(), gson.toJson(employeeEvent.getOldEmployee()),
                gson.toJson(employeeEvent.getNewEmployee())));
    }

    @Override
    @Transactional(readOnly = true)
    public List<com.mycompany.employeeevent.model.Event> findAllEvents() {
        final List<com.mycompany.employeeevent.model.Event> events = new ArrayList<>();
        for (Event eventEntity : eventRepository.findAllOrderByCreatedDate()) {
            com.mycompany.employeeevent.model.Event event = new com.mycompany.employeeevent.model.Event();
            event.setCreatedDate(eventEntity.getCreatedDate());
            event.setEventType(eventEntity.getEventType());
            event.setId(eventEntity.getId());
            event.setOldEmployee(gson.fromJson(eventEntity.getOldEmployee(), Employee.class));
            event.setNewEmployee(gson.fromJson(eventEntity.getNewEmployee(), Employee.class));
            events.add(event);
        }
        return events;
    }
}
