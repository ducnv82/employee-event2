package com.mycompany.employeeevent.controller;

import java.util.List;

import com.mycompany.employeeevent.model.Event;
import com.mycompany.employeeevent.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/events")
@Api(description = "RESTful APIs for event's operations")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping
    @ApiOperation(value = "Get all events order by createdDate", response = Event.class, responseContainer = "List", produces = "application/json")
    public List<Event> findAllEvents() {
        return eventService.findAllEvents();
    }
}
