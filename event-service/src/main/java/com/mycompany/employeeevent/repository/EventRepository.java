package com.mycompany.employeeevent.repository;

import java.util.List;
import java.util.UUID;

import com.mycompany.employeeevent.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, UUID> {

    @Query("select entityResult from #{#entityName} entityResult order by createdDate")
    List<Event> findAllOrderByCreatedDate();
}
