package com.mycompany.employeeevent.config;

import com.mycompany.employeeevent.service.EventService;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CamelRoutes extends RouteBuilder {

    @Autowired
    private EventService eventService;

    @Override
    public void configure() {

        from("activemq:queue:EMPLOYEE.EVENT?transacted=true&lazyCreateTransactionManager=false")
                .bean(eventService, "processEmployeeEvent");
    }
}
