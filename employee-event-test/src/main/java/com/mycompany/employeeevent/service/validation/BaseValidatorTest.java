package com.mycompany.employeeevent.service.validation;

import java.util.Set;

import javax.validation.ConstraintValidatorFactory;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import com.mycompany.employeeevent.service.BaseTest;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.hibernate.validator.cfg.ConstraintMapping;
import org.junit.Before;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

public abstract class BaseValidatorTest<T> extends BaseTest {

    protected boolean validatorInitialized = false;

    protected Validator validator;

    @Before
    public void setUpValidator() {
        if (!validatorInitialized) {
            validator = createValidator();
            validatorInitialized = true;
        }
    }

    protected Validator createValidator() {
        return createValidator(null);
    }

    protected Validator createValidator(final ConstraintValidatorFactory constraintValidatorFactory) {
        final HibernateValidatorConfiguration configuration =
                Validation.byProvider(HibernateValidator.class).configure();

        final ConstraintMapping constraintMapping = configuration.createConstraintMapping();
        final HibernateValidatorConfiguration validatorConfiguration = configuration.addMapping(constraintMapping);

        if (constraintValidatorFactory != null) {
            validatorConfiguration.constraintValidatorFactory(constraintValidatorFactory);
        }

        return validatorConfiguration.buildValidatorFactory().getValidator();
    }

    protected void assertNoViolation(final Set<ConstraintViolation<T>> violations) {
        assertThat(violations, empty());
    }

    protected void assertViolations(final Set<ConstraintViolation<T>> violations) {
        assertThat(violations, hasSize(greaterThan(0)));
    }

    protected void assertViolationsForProperty(final Set<ConstraintViolation<T>> violations,
                                               final String propertyName) {
        assertThat(violations, hasSize(greaterThan(0)));

        for (final ConstraintViolation<T> violation : violations) {
            assertThat(violation.getPropertyPath().toString(), is(equalTo(propertyName)));
        }
    }
}
