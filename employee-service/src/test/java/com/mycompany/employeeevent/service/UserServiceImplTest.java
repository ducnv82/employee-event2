package com.mycompany.employeeevent.service;

import java.util.List;
import java.util.Optional;

import com.mycompany.employeeevent.entity.User;
import com.mycompany.employeeevent.model.LoginRequest;
import com.mycompany.employeeevent.model.LoginResponse;
import com.mycompany.employeeevent.repository.UserRepository;
import com.mycompany.employeeevent.security.jwt.JwtTokenProvider;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;

public class UserServiceImplTest {

    @Tested
    private UserServiceImpl userService;

    @Injectable
    private UserRepository userRepository;

    @Injectable
    private AuthenticationManager authenticationManager;

    @Injectable
    private JwtTokenProvider jwtTokenProvider;

    @Test
    public void testLoadUserByUsername() {
        final String username = "admin";
        new Expectations() {
            {
                userRepository.findByUsername(username);
                result = Optional.of(new User(username));
            }
        };

        userService.loadUserByUsername(username);

        new Verifications() {{
            String userNameToVerify;
            userRepository.findByUsername(userNameToVerify = withCapture());

            assertEquals(username, userNameToVerify);
        }};
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsername_usernameNotFoundException() {
        final String username = "admin";
        new Expectations() {
            {
                userRepository.findByUsername(username);
                result = Optional.empty();
            }
        };

        userService.loadUserByUsername(username);
    }

    @Test
    public void testLogin() {
        final LoginRequest loginRequest = new LoginRequest("admin", "admin");
        final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
        final String token = "Bearer xxx";

        new Expectations() {
            {
                authenticationManager.authenticate(usernamePasswordAuthenticationToken);
                result = usernamePasswordAuthenticationToken;

                jwtTokenProvider.createToken(anyString, (List<String>) any);
                result = token;
            }
        };

        final LoginResponse loginResponse = userService.login(loginRequest);

        new Verifications() {{
            String userNameToVerify;
            jwtTokenProvider.createToken(userNameToVerify = withCapture(), (List<String>) any);

            UsernamePasswordAuthenticationToken authenticationToVerify;
            authenticationManager.authenticate(authenticationToVerify = withCapture());

            assertEquals(loginRequest.getUsername(), userNameToVerify);
            assertEquals(usernamePasswordAuthenticationToken, authenticationToVerify);
            assertEquals(token, loginResponse.getToken());
        }};
    }
}
