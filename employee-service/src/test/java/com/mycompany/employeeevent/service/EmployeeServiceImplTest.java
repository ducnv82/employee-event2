package com.mycompany.employeeevent.service;

import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import com.mycompany.employeeevent.entity.Department;
import com.mycompany.employeeevent.entity.Employee;
import com.mycompany.employeeevent.model.CreateEmployeeRequest;
import com.mycompany.employeeevent.model.UpdateEmployeeRequest;
import com.mycompany.employeeevent.repository.DepartmentRepository;
import com.mycompany.employeeevent.repository.EmployeeRepository;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.apache.camel.ProducerTemplate;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EmployeeServiceImplTest extends BaseTest {

    @Tested
    private EmployeeServiceImpl employeeService;

    @Injectable
    private DepartmentRepository departmentRepository;

    @Injectable
    private EmployeeRepository employeeRepository;

    @Injectable
    private ProducerTemplate userEventProducerTemplate;

    @Test
    public void testCreateEmployee() {
        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();

        new Expectations() {
            {
                departmentRepository.existsById(createEmployeeRequest.getDepartmentId());
                result = true;

                employeeRepository.countByEmailIgnoreCase(createEmployeeRequest.getEmail());
                result = 0;
            }
        };

        employeeService.createEmployee(createEmployeeRequest);

        new Verifications() {{
            Employee employee;
            employeeRepository.save(employee = withCapture());

            assertEquals(createEmployeeRequest.getDepartmentId(), employee.getDepartmentId());
            assertEquals(createEmployeeRequest.getEmail(), employee.getEmail());
            assertEquals(createEmployeeRequest.getBirthday(), employee.getBirthday());
            assertEquals(createEmployeeRequest.getFullName(), employee.getFullName());
        }};
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateEmployeeNonExistenceDepartment() {
        new Expectations() {
            {
                departmentRepository.existsById(anyLong);
                result = false;
            }
        };

        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();
        employeeService.createEmployee(createEmployeeRequest);
    }

    @Test(expected = EntityExistsException.class)
    public void testCreateEmployeeDuplicateEmail() {
        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();

        new Expectations() {
            {
                departmentRepository.existsById(createEmployeeRequest.getDepartmentId());
                result = true;

                employeeRepository.countByEmailIgnoreCase(createEmployeeRequest.getEmail());
                result = 1;
            }
        };

        employeeService.createEmployee(createEmployeeRequest);
    }

    @Test
    public void testFindEmployeeById() {
        final UUID id = UUID.randomUUID();
        final Employee employee = createEmployee(id);
        final Department department = createDepartment();

        new Expectations() {
            {
                employeeRepository.findById(id);
                result = Optional.of(employee);

                departmentRepository.getOne(employee.getDepartmentId());
                result = createDepartment();
            }
        };

        final com.mycompany.employeeevent.model.Employee employeeToVerify = employeeService.findEmployeeById(id);
        assertEquals(employee.getBirthday(), employeeToVerify.getBirthday());
        assertEquals(employee.getEmail(), employeeToVerify.getEmail());
        assertEquals(employee.getFullName(), employeeToVerify.getFullName());
        assertEquals(employee.getId(), employeeToVerify.getId());
        assertEquals(department.getName(), employeeToVerify.getDepartment().getName());
        assertEquals(department.getId().longValue(), employeeToVerify.getDepartment().getId());
    }

    @Test
    public void testFindEmployeeByIdNonExistence() {
        final UUID id = UUID.randomUUID();

        new Expectations() {
            {
                employeeRepository.findById(id);
                result = Optional.empty();
            }
        };

        com.mycompany.employeeevent.model.Employee employee = employeeService.findEmployeeById(id);
        assertNull(employee);
    }

    @Test
    public void testDeleteEmployeeById() {
        final UUID id = UUID.randomUUID();
        final Employee employee = createEmployee(id);

        new Expectations() {
            {
                employeeRepository.findById(id);
                result = Optional.of(employee);
            }
        };

        employeeService.deleteEmployeeById(id);

        new Verifications() {{
            Employee employeeToVerify;
            employeeRepository.delete(employeeToVerify = withCapture());

            assertEquals(employee.getId(), employeeToVerify.getId());
            assertEquals(employee.getBirthday(), employeeToVerify.getBirthday());
            assertEquals(employee.getDepartmentId(), employeeToVerify.getDepartmentId());
            assertEquals(employee.getEmail(), employeeToVerify.getEmail());
            assertEquals(employee.getFullName(), employeeToVerify.getFullName());
        }};
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteEmployeeByIdNonExistence() {
        final UUID id = UUID.randomUUID();

        new Expectations() {
            {
                employeeRepository.findById(id);
                result = Optional.empty();
            }
        };

        employeeService.deleteEmployeeById(id);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateEmployeeNonExistence() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();

        new Expectations() {
            {
                employeeRepository.existsById(updateEmployeeRequest.getId());
                result = false;
            }
        };

        employeeService.updateEmployee(updateEmployeeRequest);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateEmployeeDepartmentIdNonExistence() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();

        new Expectations() {
            {
                employeeRepository.existsById(updateEmployeeRequest.getId());
                result = true;

                departmentRepository.existsById(updateEmployeeRequest.getDepartmentId());
                result = false;
            }
        };

        employeeService.updateEmployee(updateEmployeeRequest);
    }

    @Test
    public void testUpdateEmployee() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();
        final UUID id = updateEmployeeRequest.getId();

        new Expectations() {
            {
                employeeRepository.existsById(id);
                result = true;

                departmentRepository.existsById(updateEmployeeRequest.getDepartmentId());
                result = true;

                employeeRepository.getOne(id);
                result = createEmployee(id);
            }
        };

        employeeService.updateEmployee(updateEmployeeRequest);

        new Verifications() {{
            Employee employeeToVerify;
            employeeRepository.save(employeeToVerify = withCapture());

            assertEquals(updateEmployeeRequest.getBirthday(), employeeToVerify.getBirthday());
            assertEquals(updateEmployeeRequest.getDepartmentId(), employeeToVerify.getDepartmentId());
            assertEquals(updateEmployeeRequest.getEmail(), employeeToVerify.getEmail());
            assertEquals(updateEmployeeRequest.getFullName(), employeeToVerify.getFullName());
            assertEquals(updateEmployeeRequest.getId(), employeeToVerify.getId());
        }};
    }
}
