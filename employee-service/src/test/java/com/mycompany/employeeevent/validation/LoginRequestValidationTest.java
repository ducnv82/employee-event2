package com.mycompany.employeeevent.validation;

import com.mycompany.employeeevent.model.LoginRequest;
import com.mycompany.employeeevent.service.validation.BaseValidatorTest;
import org.junit.Test;

public class LoginRequestValidationTest extends BaseValidatorTest {

    @Test
    public void testNullUsername() {
        final LoginRequest loginRequest = new LoginRequest(null, "admin");
        assertViolationsForProperty(validator.<Object>validate(loginRequest), "username");
    }

    @Test
    public void testBlankUsername() {
        final LoginRequest loginRequest = new LoginRequest(" ", "admin");
        assertViolationsForProperty(validator.<Object>validate(loginRequest), "username");
    }

    @Test
    public void testNullPassword() {
        final LoginRequest loginRequest = new LoginRequest("admin", null);
        assertViolationsForProperty(validator.<Object>validate(loginRequest), "password");
    }

    @Test
    public void testBlankPassword() {
        final LoginRequest loginRequest = new LoginRequest("admin", "  ");
        assertViolationsForProperty(validator.<Object>validate(loginRequest), "password");
    }

    @Test
    public void testValidLoginRequest() {
        final LoginRequest loginRequest = new LoginRequest("admin", "admin");
        assertNoViolation(validator.<Object>validate(loginRequest));
    }
}
