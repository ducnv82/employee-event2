package com.mycompany.employeeevent.validation;

import com.mycompany.employeeevent.model.UpdateEmployeeRequest;
import com.mycompany.employeeevent.service.validation.BaseValidatorTest;
import org.junit.Test;

public class UpdateEmployeeRequestValidationTest extends BaseValidatorTest {

    @Test
    public void testNullId() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();
        updateEmployeeRequest.setId(null);
        assertViolationsForProperty(validator.<Object>validate(updateEmployeeRequest), "id");
    }

    @Test
    public void testInvalidEmail() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();
        updateEmployeeRequest.setEmail("aaaa");
        assertViolationsForProperty(validator.<Object>validate(updateEmployeeRequest), "email");
    }

    @Test
    public void testBlankEmail() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();
        updateEmployeeRequest.setEmail(" ");
        assertViolationsForProperty(validator.<Object>validate(updateEmployeeRequest), "email");
    }

    @Test
    public void testNullDepartmentId() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();
        updateEmployeeRequest.setDepartmentId(null);
        assertViolationsForProperty(validator.<Object>validate(updateEmployeeRequest), "departmentId");
    }

    @Test
    public void testValidUpdateEmployeeRequest() {
        final UpdateEmployeeRequest updateEmployeeRequest = createUpdateEmployeeRequest();
        assertNoViolation(validator.<Object>validate(updateEmployeeRequest));
    }
}
