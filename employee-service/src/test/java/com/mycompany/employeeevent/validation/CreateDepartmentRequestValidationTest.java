package com.mycompany.employeeevent.validation;

import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import com.mycompany.employeeevent.service.validation.BaseValidatorTest;
import org.junit.Test;

public class CreateDepartmentRequestValidationTest extends BaseValidatorTest {

    @Test
    public void testNullDepartmentName() {
        assertViolationsForProperty(validator.<Object>validate(new CreateDepartmentRequest()), "name");
    }

    @Test
    public void testBlankDepartmentName() {
        assertViolationsForProperty(validator.<Object>validate(new CreateDepartmentRequest(" ")), "name");
    }

    @Test
    public void testValidDepartmentName() {
        assertNoViolation(validator.<Object>validate(new CreateDepartmentRequest("Department 1")));
    }
}
