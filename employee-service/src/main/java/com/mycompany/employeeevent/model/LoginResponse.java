package com.mycompany.employeeevent.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class LoginResponse {

    @ApiModelProperty(notes = "JSON Web Token",
                      example = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU2MDM0NTc5LCJleHAiOjE1NTYwNjMzNzl9.WYt6VxfNWqMpxsH9QJI5x4vCrpWCzi8BAOE4eejkLi8")
    private final String token;

    public LoginResponse(final String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
