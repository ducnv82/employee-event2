package com.mycompany.employeeevent.controller;

import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.employeeevent.model.CreateEmployeeRequest;
import com.mycompany.employeeevent.model.Employee;
import com.mycompany.employeeevent.model.UpdateEmployeeRequest;
import com.mycompany.employeeevent.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/employees")
@Api(description = "RESTful APIs for employee operations")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(CREATED)
    @ApiOperation(value = "Create an employee", response = Employee.class, code = 201, produces = "application/json")
    public Employee createEmployee(@ApiParam(value = "CreateEmployeeRequest object", required = true)
                                   @RequestBody @Valid final CreateEmployeeRequest request) {
        return employeeService.createEmployee(request);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find an employee by id. Response is null if not found", response = Employee.class, produces = "application/json")
    public Employee findEmployeeById(@ApiParam(required = true) @PathVariable @Valid @NotNull final UUID id) {
        return employeeService.findEmployeeById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete an employee by id.", response = Void.class, produces = "application/json")
    public void deleteEmployeeById(@ApiParam(required = true) @PathVariable @Valid @NotNull final UUID id) {
        employeeService.deleteEmployeeById(id);
    }

    @PutMapping
    @ApiOperation(value = "Update an employee", response = Employee.class, produces = "application/json")
    public Employee updateEmployee(@ApiParam(value = "UpdateEmployeeRequest object", required = true)
                                   @RequestBody @Valid final UpdateEmployeeRequest request) {
        return employeeService.updateEmployee(request);
    }
}
