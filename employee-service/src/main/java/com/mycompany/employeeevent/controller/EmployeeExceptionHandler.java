package com.mycompany.employeeevent.controller;

import javax.servlet.http.HttpServletRequest;

import com.mycompany.employeeevent.model.ErrorResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import static java.lang.System.currentTimeMillis;
import static org.springframework.http.HttpStatus.FORBIDDEN;

//@RestController
//@RestControllerAdvice
public class EmployeeExceptionHandler extends RestResponseEntityExceptionHandler implements ErrorController {

    @ExceptionHandler({AccessDeniedException.class, AuthenticationException.class})
    protected ResponseEntity<Object> handleAuthenticationException(final RuntimeException ex, final WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponse(FORBIDDEN, request, ex.getMessage()), new HttpHeaders(),
                                       FORBIDDEN, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionError(final Exception exception, final HttpServletRequest request) {
        ResponseEntity<Object> errorResponse;
        if (exception instanceof AuthenticationException) {
            errorResponse = handleAuthenticationException((AuthenticationException) exception, request);
        } else {
            errorResponse = handleOtherException(exception, request);
        }

        return errorResponse;
    }

    protected ResponseEntity<Object> handleAuthenticationException(final AuthenticationException ex, final HttpServletRequest request) {
        final HttpStatus httpStatus = FORBIDDEN;
        final ErrorResponse errorResponse = new ErrorResponse(currentTimeMillis(), httpStatus.value(),
                                                              httpStatus.getReasonPhrase(), ex.getMessage(),
                                                              request.getRequestURI());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }
}
