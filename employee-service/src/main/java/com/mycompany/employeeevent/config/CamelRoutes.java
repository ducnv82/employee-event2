package com.mycompany.employeeevent.config;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CamelRoutes extends RouteBuilder {

    @Override
    public void configure() {

        from("direct:employee-event").to("activemq:queue:EMPLOYEE.EVENT?transacted=true&lazyCreateTransactionManager=false");
    }
}
