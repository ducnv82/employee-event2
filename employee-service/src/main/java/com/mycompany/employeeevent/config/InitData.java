package com.mycompany.employeeevent.config;

import com.mycompany.employeeevent.entity.User;
import com.mycompany.employeeevent.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.collect.ImmutableList.of;

@Component
public class InitData implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void run(final ApplicationArguments args) {
        userRepository.save(new User("admin", passwordEncoder.encode("admin"), of("ROLE_USER", "ROLE_ADMIN")));
    }
}
