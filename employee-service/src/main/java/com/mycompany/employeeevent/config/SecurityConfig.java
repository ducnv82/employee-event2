package com.mycompany.employeeevent.config;

import com.mycompany.employeeevent.security.jwt.JwtConfigurer;
import com.mycompany.employeeevent.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .httpBasic().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
                .authorizeRequests()
                //.antMatchers("/auth/signin").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers(POST, "/api/departments/**").authenticated()
                .antMatchers(POST, "/api/employees/**").authenticated()
                .antMatchers(DELETE, "/api/employees/**").authenticated()
                .antMatchers(PUT, "/api/employees/**").authenticated()
                .anyRequest().permitAll()
            .and()
            .apply(new JwtConfigurer(jwtTokenProvider));

        http.headers().frameOptions().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}

