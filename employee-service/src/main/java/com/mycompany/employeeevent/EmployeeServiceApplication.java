package com.mycompany.employeeevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(EmployeeServiceApplication.class, args);
    }
}
