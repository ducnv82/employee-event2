package com.mycompany.employeeevent.service;

import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.employeeevent.model.CreateEmployeeRequest;
import com.mycompany.employeeevent.model.Employee;
import com.mycompany.employeeevent.model.UpdateEmployeeRequest;

public interface EmployeeService {

    Employee createEmployee(@Valid CreateEmployeeRequest request);

    Employee findEmployeeById(@Valid @NotNull UUID id);

    void deleteEmployeeById(@Valid @NotNull UUID id);

    Employee updateEmployee(@Valid UpdateEmployeeRequest request);
}
