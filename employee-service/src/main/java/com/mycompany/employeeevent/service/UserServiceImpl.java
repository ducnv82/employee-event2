package com.mycompany.employeeevent.service;

import javax.validation.Valid;

import com.mycompany.employeeevent.model.LoginRequest;
import com.mycompany.employeeevent.model.LoginResponse;
import com.mycompany.employeeevent.repository.UserRepository;
import com.mycompany.employeeevent.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import static java.util.stream.Collectors.toList;

@Service
@Validated
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username: '" + username + "' not found"));
    }

    @Override
    @Transactional(readOnly = true)
    public LoginResponse login(final @Valid LoginRequest loginRequest) {
        final String username = loginRequest.getUsername();

        final Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username, loginRequest.getPassword()));
        final String token = jwtTokenProvider.createToken(username, authentication.getAuthorities().stream().map(
                GrantedAuthority::getAuthority).collect(toList()));

        return new LoginResponse(token);
    }
}
