package com.mycompany.employeeevent.service;

import javax.validation.Valid;

import com.mycompany.employeeevent.model.LoginRequest;
import com.mycompany.employeeevent.model.LoginResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    LoginResponse login(final @Valid LoginRequest loginRequest);
}
