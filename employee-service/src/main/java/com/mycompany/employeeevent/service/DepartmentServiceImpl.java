package com.mycompany.employeeevent.service;

import javax.validation.Valid;

import com.mycompany.employeeevent.entity.Department;
import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import com.mycompany.employeeevent.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    @Transactional
    public com.mycompany.employeeevent.model.Department createDepartment(@Valid final CreateDepartmentRequest request) {
        return com.mycompany.employeeevent.model.Department.of(departmentRepository.save(new Department(request.getName())));
    }
}
