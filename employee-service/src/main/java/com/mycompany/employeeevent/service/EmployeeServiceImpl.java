package com.mycompany.employeeevent.service;

import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.employeeevent.entity.Department;
import com.mycompany.employeeevent.entity.Employee;
import com.mycompany.employeeevent.model.CreateEmployeeRequest;
import com.mycompany.employeeevent.model.EmployeeEvent;
import com.mycompany.employeeevent.model.UpdateEmployeeRequest;
import com.mycompany.employeeevent.repository.DepartmentRepository;
import com.mycompany.employeeevent.repository.EmployeeRepository;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import static com.mycompany.employeeevent.model.EventType.CREATED;
import static com.mycompany.employeeevent.model.EventType.DELETED;
import static com.mycompany.employeeevent.model.EventType.UPDATED;
import static org.springframework.beans.BeanUtils.copyProperties;

@Service
@Validated
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Produce(uri = "direct:employee-event")
    private ProducerTemplate employeeEventProducerTemplate;

    @Override
    @Transactional
    public com.mycompany.employeeevent.model.Employee createEmployee(@Valid final CreateEmployeeRequest request) {
        final Long departmentId = request.getDepartmentId();
        if (!departmentRepository.existsById(departmentId)) {
            throw new EntityNotFoundException("Cannot find department with id '" + departmentId.toString() + "'");
        }

        final String email = request.getEmail();
        if (employeeRepository.countByEmailIgnoreCase(email) > 0) {
            throw new EntityExistsException("A user with email '" + email + "' already exists");
        }

        final com.mycompany.employeeevent.model.Employee employee =
                com.mycompany.employeeevent.model.Employee.of(employeeRepository.save(Employee.of(request)));
        employeeEventProducerTemplate.sendBody(new EmployeeEvent(CREATED, null, employee));
        return employee;
    }

    @Override
    @Transactional(readOnly = true)
    public com.mycompany.employeeevent.model.Employee findEmployeeById(@Valid @NotNull final UUID id) {
        final Optional<Employee> employeeEntity = employeeRepository.findById(id);
        if (employeeEntity.isPresent()) {
            final com.mycompany.employeeevent.model.Employee employee =
                    com.mycompany.employeeevent.model.Employee.of(employeeEntity.get());
            final Department departmentEntity = departmentRepository.getOne(employee.getDepartmentId());
            employee.setDepartment(com.mycompany.employeeevent.model.Department.of(departmentEntity));
            return employee;
        }

        return null;
    }

    @Override
    @Transactional
    public void deleteEmployeeById(@Valid @NotNull final UUID id) {
        final Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isPresent()) {
            final Employee employeeEntity = optionalEmployee.get();
            final com.mycompany.employeeevent.model.Employee oldEmployee =
                    com.mycompany.employeeevent.model.Employee.of(employeeEntity);
            employeeRepository.delete(employeeEntity);
            employeeEventProducerTemplate.sendBody(new EmployeeEvent(DELETED, oldEmployee, null));
        } else {
            throw new EntityNotFoundException("Cannot find user with id '" + id.toString() + "'");
        }
    }

    @Override
    @Transactional
    public com.mycompany.employeeevent.model.Employee updateEmployee(@Valid final UpdateEmployeeRequest request) {
        final UUID id = request.getId();
        if (!employeeRepository.existsById(id)) {
            throw new EntityNotFoundException("Cannot find user with id '" + id.toString() + "'");
        }

        final Long departmentId = request.getDepartmentId();
        if (!departmentRepository.existsById(departmentId)) {
            throw new EntityNotFoundException("Cannot find department with id '" + departmentId.toString() + "'");
        }

        final Employee employeeEntity = employeeRepository.getOne(id);
        final com.mycompany.employeeevent.model.Employee oldEmployee =
                com.mycompany.employeeevent.model.Employee.of(employeeEntity);
        copyProperties(request, employeeEntity);
        employeeRepository.save(employeeEntity);
        final com.mycompany.employeeevent.model.Employee newEmployee =
                com.mycompany.employeeevent.model.Employee.of(employeeEntity);

        employeeEventProducerTemplate.sendBody(new EmployeeEvent(UPDATED, oldEmployee, newEmployee));
        return newEmployee;
    }
}
