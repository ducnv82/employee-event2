package com.mycompany.employeeevent.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import static com.google.common.collect.ImmutableMap.of;
import static java.lang.System.currentTimeMillis;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.util.StringUtils.hasText;

public class JwtTokenFilter extends GenericFilterBean {

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(final JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain filterChain)
            throws IOException, ServletException {

        final String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        try {
            if (hasText(token) && jwtTokenProvider.validateToken(token)) {
                final Authentication auth = jwtTokenProvider.getAuthentication(token);
                if (auth != null) {
                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            }
        } catch (final AuthenticationException e) {
            final HttpServletResponse response = (HttpServletResponse) res;
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(403);
            final String error = jwtTokenProvider.getGson()
                    .toJson(of( "timestamp", currentTimeMillis(),
                                "status",    FORBIDDEN.value(),
                                "error",     FORBIDDEN.getReasonPhrase(),
                                "message",   e.getMessage(),
                                "path",      ((HttpServletRequest) req).getRequestURI()));
            response.getWriter().write(error);
            return;
        }

        filterChain.doFilter(req, res);
    }
}
