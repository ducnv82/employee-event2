package com.mycompany.employeeevent.controller;


import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.mycompany.employeeevent.EmployeeServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.isEmptyString;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeServiceApplication.class, webEnvironment = DEFINED_PORT)
public class LoginControllerIT extends BaseEmployeeIT {

    @Test
    public void testLogin_invalidPassword() {
        final Map<String, String> loginRequest = ImmutableMap.of("username", "admin", "password", "xxxx");

        given().body(loginRequest)
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(403);
    }

    @Test
    public void testLogin_usernameNotFound() {
        final Map<String, String> loginRequest = ImmutableMap.of("username", "xxxx", "password", "admin");

        given().body(loginRequest)
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(403);
    }

    @Test
    public void testLogin() {
        final Map<String, String> loginRequest = ImmutableMap.of("username", "admin", "password", "admin");

        given().body(loginRequest)
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(200)
                .body("token", not(isEmptyString()));
    }

    @Test
    public void testLogin_blankUsername() {
        final Map<String, String> loginRequest = ImmutableMap.of("username", "", "password", "admin");

        given().body(loginRequest)
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("username"))
                .body("path", equalTo("/login"))
                .body("status", equalTo(400));
    }

    @Test
    public void testLogin_blankPassword() {
        final Map<String, String> loginRequest = ImmutableMap.of("username", "admin", "password", "");

        given().body(loginRequest)
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("password"))
                .body("path", equalTo("/login"))
                .body("status", equalTo(400));
    }

    @Test
    public void testLogin_blankUsernameAndPassword() {
        final Map<String, String> loginRequest = ImmutableMap.of("username", "", "password", "");

        given().body(loginRequest)
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("username"))
                .body("message", containsString("password"))
                .body("path", equalTo("/login"))
                .body("status", equalTo(400));
    }
}
