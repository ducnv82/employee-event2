package com.mycompany.employeeevent.controller;

import com.mycompany.employeeevent.EmployeeServiceApplication;
import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeServiceApplication.class, webEnvironment = DEFINED_PORT)
public class DepartmentControllerIT extends BaseEmployeeIT {

    @Test
    public void testCreateDepartment_forbidden() {
        final CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest();
        createDepartmentRequest.setName("department 2");

        given().body(createDepartmentRequest)
                .contentType(JSON).accept(JSON)
                .when().post("http://localhost:8080/api/departments").then().statusCode(403)
                .body("error", equalTo(FORBIDDEN.getReasonPhrase()))
                .body("path", equalTo("/api/departments"))
                .body("status", equalTo(403));
    }

    @Test
    public void testCreateDepartment_blankName() {
        final CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest();
        createDepartmentRequest.setName("       ");

        given().body(createDepartmentRequest)
                .contentType(JSON).accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/departments").then().statusCode(400)
                .body("error", equalTo("Bad Request"))
                .body("message", containsString("name"))
                .body("path", equalTo("/api/departments"))
                .body("status", equalTo(400));
    }

    @Test
    public void testCreateDepartment_autoIncrementId() {
        final CreateDepartmentRequest createDepartment2Request = new CreateDepartmentRequest();
        createDepartment2Request.setName("department 2");

        given().body(createDepartment2Request)
                .contentType(JSON).accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/departments").then().statusCode(201)
                .body("id", equalTo(2))
                .body("name", equalTo(createDepartment2Request.getName()));
    }
}
