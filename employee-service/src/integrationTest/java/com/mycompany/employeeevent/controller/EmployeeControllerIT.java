package com.mycompany.employeeevent.controller;


import java.util.UUID;

import com.google.common.collect.ImmutableMap;
import com.mycompany.employeeevent.EmployeeServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static java.util.UUID.randomUUID;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeServiceApplication.class, webEnvironment = DEFINED_PORT)
public class EmployeeControllerIT extends BaseEmployeeIT {

    @Test
    public void testFindEmployeeById() {
        final String id = setupAndTestCreateEmployee();

        given().contentType(JSON)
                .accept(JSON)
                .when().get("http://localhost:8080/api/employees/" + id).then().statusCode(200)
                .body("id", equalTo(id))
                .body("email", equalTo(createEmployeeRequest.get("email")))
                .body("fullName", equalTo(createEmployeeRequest.get("fullName")))
                .body("departmentId", equalTo(createEmployeeRequest.get("departmentId")))
                .body("birthday", equalTo(createEmployeeRequest.get("birthday")))
                .body("department.id", equalTo(1))
                .body("department.name", equalTo(createDepartmentRequest.getName()));
    }

    @Test
    public void testUpdateEmployee() {
        final String id = setupAndTestCreateEmployee();

        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05", "departmentId", 1,
                                "email", "robin.hood5@aol.com", "fullName", "Robin Hood 5", "id", id);
        given().body(updateEmployeeRequest).contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(200)
                .body("id", equalTo(id))
                .body("email", equalTo(updateEmployeeRequest.get("email")))
                .body("fullName", equalTo(updateEmployeeRequest.get("fullName")))
                .body("departmentId", equalTo(updateEmployeeRequest.get("departmentId")))
                .body("birthday", equalTo(updateEmployeeRequest.get("birthday")));
    }

    @Test
    public void testUpdateEmployee_forbidden() {
        given().contentType(JSON).accept(JSON)
                .when().put("http://localhost:8080/api/employees").then().statusCode(403);
    }

    @Test
    public void testCreateEmployee_forbidden() {
        given().contentType(JSON).accept(JSON)
                .when().post("http://localhost:8080/api/employees").then().statusCode(403);
    }

    @Test
    public void testDeleteEmployee_forbidden() {
        given().contentType(JSON).accept(JSON)
                .when().delete("http://localhost:8080/api/employees/" + randomUUID()).then().statusCode(403);
    }

    @Test
    public void testUpdateEmployee_invalidAuthenticationJwtToken() {
        given().contentType(JSON).accept(JSON).auth().oauth2(jwtToken + "a")
                .when().put("http://localhost:8080/api/employees").then().statusCode(403);
    }

    @Test
    public void testCreateEmployee_invalidAuthenticationJwtToken() {
        given().contentType(JSON).accept(JSON).auth().oauth2(jwtToken + "a")
                .when().post("http://localhost:8080/api/employees").then().statusCode(403);
    }

    @Test
    public void testDeleteEmployee_invalidAuthenticationJwtToken() {
        given().contentType(JSON).accept(JSON).auth().oauth2(jwtToken + "a")
                .when().delete("http://localhost:8080/api/employees/" + randomUUID()).then().statusCode(403);
    }

    @Test
    public void testUpdateEmployee_blankAuthenticationJwtToken() {
        given().contentType(JSON).accept(JSON).auth().oauth2( "  ")
                .when().put("http://localhost:8080/api/employees").then().statusCode(403);
    }

    @Test
    public void testCreateEmployee_blankAuthenticationJwtToken() {
        given().contentType(JSON).accept(JSON).auth().oauth2("  ")
                .when().post("http://localhost:8080/api/employees").then().statusCode(403);
    }

    @Test
    public void testDeleteEmployee_blankAuthenticationJwtToken() {
        given().contentType(JSON).accept(JSON).auth().oauth2("  ")
                .when().delete("http://localhost:8080/api/employees/" + randomUUID()).then().statusCode(403);
    }

    @Test
    public void testUpdateEmployee_nonExistenceUser() {
        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05", "departmentId", 1, "email", "robin.hood5@aol.com",
                                "fullName", "Robin Hood 5", "id", randomUUID());
        given().body(updateEmployeeRequest).contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(404)
                .body("error", equalTo(NOT_FOUND.getReasonPhrase()))
                .body("message", containsString("id"))
                .body("message", containsString(updateEmployeeRequest.get("id").toString()))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(404));
    }

    @Test
    public void testDeleteEmployeeById() {
        final String id = setupAndTestCreateEmployee();

        given().contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().delete("http://localhost:8080/api/employees/" + id).then().statusCode(200);

        final String responseBody = given().contentType(JSON)
                .accept(JSON)
                .when().get("http://localhost:8080/api/employees/" + id).then().statusCode(200)
                .extract().asString();

        assertThat(responseBody, isEmptyString());
    }

    @Test
    public void testCreateEmployee_duplicateEmail() {
        setupAndTestCreateEmployee();

        createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01", "departmentId", 1,
                                "email", "robin.hood@aol.com", "fullName", "Robin Hood");

        given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(409)
                .body("error", equalTo(CONFLICT.getReasonPhrase()))
                .body("message", containsString("email"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(409));
    }

    @Test
    public void testCreateEmployee_blankEmail() {
        final ImmutableMap<String, Object>
                createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01", "departmentId", 1,
                                "email", "", "fullName", "Robin Hood");
        given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("email"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testCreateEmployee_invalidEmail() {
        final ImmutableMap<String, Object>
                createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01", "departmentId", 1,
                                "email", "aaa", "fullName", "Robin Hood");
        given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("email"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testCreateEmployee_nullDepartmentId() {
        final ImmutableMap<String, Object>
                createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01",
                                "email", "aaa", "fullName", "Robin Hood");
        given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("departmentId"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testCreateEmployee_notFoundDepartmentId() {
        final ImmutableMap<String, Object>
                createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01", "departmentId", 2,
                                "email", "employee1@gmail.com", "fullName", "Robin Hood");
        given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(404)
                .body("error", equalTo(NOT_FOUND.getReasonPhrase()))
                .body("message", containsString("department"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(404));
    }

    @Test
    public void testCreateEmployee_invalidBirthday() {
        final ImmutableMap<String, Object>
                createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-dx", "departmentId", 1,
                                "email", "a@gmail.com", "fullName", "Robin Hood");
        given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("birthday"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testFindEmployeeById_nonExistenceUser() {
        final String responseBody = given().contentType(JSON)
                .accept(JSON)
                .when().get("http://localhost:8080/api/employees/" + randomUUID()).then().statusCode(200)
                .extract().asString();

        assertThat(responseBody, isEmptyString());
    }

    @Test
    public void testDeleteEmployeeById_nonExistenceUser() {
        final UUID randomId = randomUUID();

        given().contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().delete("http://localhost:8080/api/employees/" + randomId).then().statusCode(404)
                .body("error", equalTo(NOT_FOUND.getReasonPhrase()))
                .body("message", notNullValue())
                .body("path", equalTo("/api/employees/" + randomId))
                .body("status", equalTo(404));
    }

    @Test
    public void testUpdateEmployee_blankEmail() {
        final String id = setupAndTestCreateEmployee();

        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05", "departmentId", 1,
                                "email", "", "fullName", "Robin Hood 5", "id", id);
        given().body(updateEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("email"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testUpdateEmployee_invalidEmail() {
        final String id = setupAndTestCreateEmployee();

        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05", "departmentId", 1,
                                "email", "aaa", "fullName", "Robin Hood 5", "id", id);
        given().body(updateEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("email"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testUpdateEmployee_nullDepartmentId() {
        final String id = setupAndTestCreateEmployee();

        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05",
                                "email", "employee2@gmail.com", "fullName", "Robin Hood 5", "id", id);
        given().body(updateEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("departmentId"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }

    @Test
    public void testUpdateEmployee_notFoundDepartmentId() {
        final String id = setupAndTestCreateEmployee();

        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05", "departmentId", 2,
                                "email", "employee2@gmail.com", "fullName", "Robin Hood 5", "id", id);
        given().body(updateEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(404)
                .body("error", equalTo(NOT_FOUND.getReasonPhrase()))
                .body("message", containsString("department"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(404));
    }

    @Test
    public void testUpdateEmployee_invalidBirthday() {
        final String id = setupAndTestCreateEmployee();

        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-75-99", "departmentId", 1,
                                "email", "employee2@gmail.com", "fullName", "Robin Hood 5", "id", id);
        given().body(updateEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(400)
                .body("error", equalTo(BAD_REQUEST.getReasonPhrase()))
                .body("message", containsString("birthday"))
                .body("path", equalTo("/api/employees"))
                .body("status", equalTo(400));
    }
}
