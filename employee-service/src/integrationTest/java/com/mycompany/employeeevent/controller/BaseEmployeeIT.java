package com.mycompany.employeeevent.controller;

import com.google.common.collect.ImmutableMap;
import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public abstract class BaseEmployeeIT {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    protected CreateDepartmentRequest createDepartmentRequest;
    protected ImmutableMap<String, Object> createEmployeeRequest;

    protected String jwtToken;

    @Before
    public void setUp() {
        jwtToken = getJwtToken();

        setupAndTestCreateDepartment();

        createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01", "departmentId", 1,
                                "email", "robin.hood@aol.com", "fullName", "Robin Hood");
    }

    @After
    public void tearDown() {
        jdbcTemplate.update("truncate table department");
        jdbcTemplate.update("alter table department alter column id RESTART WITH 1");
        jdbcTemplate.update("delete from employee");
        jdbcTemplate.update("delete from event");
    }

    protected void setupAndTestCreateDepartment() {
        createDepartmentRequest = new CreateDepartmentRequest();
        createDepartmentRequest.setName("department 1");

        given().body(createDepartmentRequest)
                .contentType(JSON).accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/departments").then().statusCode(201)
                .body("id", equalTo(1))
                .body("name", equalTo(createDepartmentRequest.getName()));
    }

    protected String setupAndTestCreateEmployee() {
        final ImmutableMap<String, Object>
                createEmployeeRequest =
                ImmutableMap.of("birthday", "1900-01-01", "departmentId", 1,
                                "email", "robin.hood@aol.com", "fullName", "Robin Hood");
        final String id = given().body(createEmployeeRequest)
                .contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().post("http://localhost:8080/api/employees").then().statusCode(201)
                .body("id", notNullValue())
                .body("email", equalTo(createEmployeeRequest.get("email")))
                .body("fullName", equalTo(createEmployeeRequest.get("fullName")))
                .body("departmentId", equalTo(createEmployeeRequest.get("departmentId")))
                .body("birthday", equalTo(createEmployeeRequest.get("birthday")))
                .extract().path("id");

        return id;
    }

    private String getJwtToken() {
        final String token = given().body(ImmutableMap.of("username", "admin", "password", "admin"))
                .contentType(JSON)
                .accept(JSON)
                .when().post("http://localhost:8080/login").then().statusCode(200)
                .body("token", notNullValue())
                .extract().path("token");

        return token;
    }
}
